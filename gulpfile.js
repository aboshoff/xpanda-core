(function () {
  'use strict';

  var gulp = require('gulp');
  var sass = require('gulp-sass');
  var minify = require('gulp-minify');

  gulp.task('sass', function () {
    gulp.src('./scss/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'));
  });
 
  gulp.task('compress', function() {
    gulp.src('js/jquery.xpanda.js')
      .pipe(minify({
          ext:{
            min:'.min.js'
          },
          exclude: ['tasks']
      }))
      .pipe(gulp.dest('js'))
  });

  gulp.task('watch', function () {
    gulp.watch(['scss/**/*.scss', 'scss/*.scss', 'js/*.js'], ['compress']); //, 'sass']);
  });

}());
