(function ($) {

    'use strict';

    /*jslint browser:true */
    /*jslint devel:true */
    /*global $, jQuery, alert, console*/

    $.fn.xpanda = function (settings) {

        // Default settings.
        var self = this,
            defaults = $.extend(true, {
                breakpoints: {
                    xs: {
                        minWidth: 0,
                        itemsPerRow: 2
                    },
                    sm: {
                        minWidth: 576,
                        itemsPerRow: 4
                    },
                    md: {
                        minWidth: 768,
                        itemsPerRow: 6
                    },
                    lg: {
                        minWidth: 992,
                        itemsPerRow: 8
                    },
                    xl: {
                        minWidth: 1200,
                        itemsPerRow: 10
                    },
                    xx: {
                        minWidth: 1400,
                        itemsPerRow: 12
                    }
                },
                element: {
                    selector: ".x-item",
                    equalThumbSizes: true,
                    wrapperClass: "x-item-wrap",
                    toClone: ".x-content",
                    spaceBetweenItems: 10,
                    spaceBetweenRows: 10
                },
                placeholder: {
                    class: "x-placeholder",
                    desktopScrollTo: true,
                    mobileScrollTo: false,
                    marginTop: 10,
                    marginSides: 10,
                    showControls: true
                },
                spacer: {
                    class: "x-spacer-inside"
                }
            }, settings);

        return self.each(function () {

            // Namespace.
            var APP = {};

            // Sub namespace functions.
            APP.GO = {

                // Global variables.
                globalVar: function () {

                    // Fixed.
                    APP.GO.win = $(window);
                    APP.GO.doc = $(document);
                    APP.GO.html = $('html');
                    APP.GO.userAgent = navigator.userAgent;

                    APP.GO.container = self; // container

                    APP.GO.item = $(String(defaults.element.selector)); // item
                    APP.GO.itemLength = APP.GO.item.length; // item length

                    // Dynamic.
                    APP.GO.winWidth = APP.GO.win.width(); // window width
                    APP.GO.winHeight = APP.GO.win.height(); // window height
                    APP.GO.winTop = APP.GO.win.scrollTop(); // window scroll top

                    APP.GO.containerWidth = APP.GO.container.width(); // container width
                    APP.GO.containerWidthCached = APP.GO.container.width(); // cached container width
                    APP.GO.columnCount = 0; // columns
                    APP.GO.updatedColumnCount = 0; // updated columns
                    APP.GO.placeholderCount = 1; // placeholders

                    APP.GO.throttleResizeFunc = _.throttle(APP.GO.updateResizeThrottle, 1);
                    APP.GO.throttleScrollFunc = _.throttle(APP.GO.updateScrollThrottle, 10);

                },

                // Initial load.
                initialLoad: function () {

                    // add x-initiated class to main selector and spacer html
                    self.addClass('x-initiated');
                    $('.x-spacer-outside').addClass('x-initiated');

                },

                // Get column count.
                getColumns: function () {

                    if (APP.GO.containerWidth > defaults.breakpoints.xx.minWidth) {
                        APP.GO.columnCount = defaults.breakpoints.xx.itemsPerRow;
                    } else if (APP.GO.containerWidth <= defaults.breakpoints.xx.minWidth && APP.GO.containerWidth > defaults.breakpoints.xl.minWidth) {
                        APP.GO.columnCount = defaults.breakpoints.xl.itemsPerRow;
                    } else if (APP.GO.containerWidth <= defaults.breakpoints.xl.minWidth && APP.GO.containerWidth > defaults.breakpoints.lg.minWidth) {
                        APP.GO.columnCount = defaults.breakpoints.lg.itemsPerRow;
                    } else if (APP.GO.containerWidth <= defaults.breakpoints.lg.minWidth && APP.GO.containerWidth > defaults.breakpoints.md.minWidth) {
                        APP.GO.columnCount = defaults.breakpoints.md.itemsPerRow;
                    } else if (APP.GO.containerWidth <= defaults.breakpoints.md.minWidth && APP.GO.containerWidth > defaults.breakpoints.sm.minWidth) {
                        APP.GO.columnCount = defaults.breakpoints.sm.itemsPerRow;
                    } else if (APP.GO.containerWidth <= defaults.breakpoints.sm.minWidth && APP.GO.containerWidth > defaults.breakpoints.xs.minWidth) {
                        APP.GO.columnCount = defaults.breakpoints.xs.itemsPerRow;
                    } else {
                        console.log('error with breakpoints');
                    }

                },

                // Create placeholders.
                createPlaceholders: function () {

                    // get column count
                    APP.GO.updatedColumnCount = APP.GO.columnCount;

                    // reset placeholder count
                    APP.GO.placeholderCount = 1;

                    APP.GO.item.each(function () {

                        // variables
                        var $self = $(this),
                            itemIndex = APP.GO.item.index($self),
                            remainder = itemIndex % APP.GO.updatedColumnCount,
                            placeholderMarkup = $('<div/>', {
                                'class': defaults.placeholder.class,
                                'data-until': APP.GO.updatedColumnCount * APP.GO.placeholderCount
                            }),
                            dataUntil = APP.GO.updatedColumnCount * APP.GO.placeholderCount;

                        // assign data-index and data-group attributes
                        $self.attr('data-index', itemIndex + 1).attr('data-group', dataUntil);

                        // insert placeholders after each last item per row
                        if (remainder === (APP.GO.updatedColumnCount - 1) || itemIndex === (APP.GO.itemLength - 1)) {

                            $self.after(placeholderMarkup);

                            $(defaults.element.selector + '[data-group="' + dataUntil + '"]').wrapAll('<div class="' + defaults.element.wrapperClass + '" style="margin: 0 ' + (defaults.element.spaceBetweenItems / 2) + 'px"></div>');

                            APP.GO.placeholderCount = APP.GO.placeholderCount + 1;

                        }

                    });

                },

                // Destroy placeholders.
                destroyPlaceholders: function () {

                    // unwrap items and remove placeholders
                    APP.GO.item.unwrap();
                    $('.' + defaults.placeholder.class).remove().removeAttr('style');

                },

                // Style items.
                styleItems: function () {

                    APP.GO.item.each(function () {

                        // variables
                        var $self = $(this),
                            img = $self.find('img'),
                            iWidth = img.width(), // for flex calculation
                            iHeight = img.height(), // for flex calculation
                            flexValue = iWidth / iHeight, // calculate flex basis
                            hori = (defaults.element.spaceBetweenItems / 2) + 'px', // calculate horizontal margins
                            vert = defaults.element.spaceBetweenRows + 'px'; // calculate vertical margins

                        // determine flex values
                        if (APP.GO.updatedColumnCount === 1 || defaults.element.equalThumbSizes === true) {
                            $self.css({ 'flex-grow': 1 });
                        } else {
                            $self.css({ 'flex-grow': flexValue });
                        }

                        $self.css({
                            'flex-shrink': 1,
                            'flex-basis': '0%',
                            'margin-top': vert,
                            'margin-bottom': 0,
                            'margin-right': hori,
                            'margin-left': hori
                        });

                    });

                    // adding bottom margin
                    APP.GO.container.css('margin-bottom', defaults.element.spaceBetweenItems);

                },

                // Create spacer.
                createSpacer: function () {

                    // variables
                    var originalSpacer = $('.x-spacer-outside'),
                        createdSpacer = $('.' + defaults.spacer.class);

                    // hide original spacer
                    originalSpacer.hide();

                    // remove previously created spacer
                    createdSpacer.remove();

                    APP.GO.item.each(function () {

                        // variables
                        var itemIndex = APP.GO.item.index(this);

                        // create spacer with styles if item length of last row not equal to column count
                        if (itemIndex === (APP.GO.itemLength - 1)) {

                            var lastPlaceholderUntil = $('.' + defaults.placeholder.class).last().attr('data-until'),
                                lastRowItems = $(defaults.element.selector + '[data-group="' + lastPlaceholderUntil + '"]'),
                                lastRowItemsLength = lastRowItems.length,
                                spacesToFill = APP.GO.columnCount - lastRowItemsLength,
                                paddingFix = ((spacesToFill - 1) * defaults.element.spaceBetweenItems) / 2,
                                lastRow = $('.' + defaults.element.wrapperClass).last(),
                                spacerMarkup = $('<div/>', {
                                    'class': defaults.spacer.class
                                }).css({
                                    'flex-grow': spacesToFill,
                                    'flex-shrink': 1,
                                    'flex-basis': '0%',
                                    'margin-top': defaults.element.spaceBetweenRows + 'px',
                                    'margin-left': (defaults.element.spaceBetweenItems / 2),
                                    'margin-right': (defaults.element.spaceBetweenItems / 2),
                                    'padding-left': paddingFix,
                                    'padding-right': paddingFix
                                });

                            // append to last row
                            if (lastRowItemsLength < APP.GO.columnCount) {
                                spacerMarkup.appendTo(lastRow);
                            } else {
                                originalSpacer.show().css({
                                    'width': APP.GO.containerWidth - (defaults.element.spaceBetweenItems * 2) + 'px',
                                    'margin-left': defaults.element.spaceBetweenItems + 'px',
                                    'margin-right': defaults.element.spaceBetweenItems + 'px',
                                    'margin-bottom': defaults.element.spaceBetweenRows + 'px'
                                });
                            }

                        }

                    });

                },

                // Add content to spacer.
                addSpacerContent: function () {

                    // variables
                    var spacerWrap = $('.x-spacer-outside'),
                        spacerContent = spacerWrap.html();

                    function isEmpty(el) {
                        return !$.trim(el.html());
                    }

                    if (isEmpty(spacerWrap)) {
                        // add empty class
                        $('.' + defaults.spacer.class).addClass('x-spacer-empty');
                    } else {
                        // insert content
                        $('.' + defaults.spacer.class).html(spacerContent);
                    }

                },

                // Trigger on resize (throttle).
                updateResizeThrottle: function () {

                    // Dynamic.
                    APP.GO.containerWidth = APP.GO.container.width(); // container width

                    // Variables.
                    var currentActive = $('.x-is-active'),
                        dataIndex = currentActive.attr('data-index');

                    // If column count has changed.
                    if (APP.GO.updatedColumnCount !== APP.GO.columnCount) {

                        APP.GO.destroyPlaceholders();
                        APP.GO.createPlaceholders();
                        APP.GO.styleItems();

                    }

                    // update spacer
                    APP.GO.createSpacer();
                    APP.GO.addSpacerContent();

                    // If item was expanded before the resize.
                    if (currentActive.length > 0) {

                        if (APP.GO.containerWidth !== APP.GO.containerWidthCached) {

                            // Dynamic.
                            APP.GO.containerWidthCached = APP.GO.container.width(); // update cached container width

                            // remove class
                            currentActive.removeClass('x-is-active');
                            // trigger click
                            $(defaults.element.selector + '[data-index="' + dataIndex + '"]').trigger('click', [true]);

                        }

                        // add placeholder styles
                        APP.GO.styleActivePlaceholder();

                    }

                    // Update column count.
                    APP.GO.getColumns();
                    APP.GO.lazyLoadThumb();

                },

                // Trigger on scroll (throttle).
                updateScrollThrottle: function () {

                    // Dynamic.
                    APP.GO.winHeight = APP.GO.win.height(); // window height
                    APP.GO.winTop = APP.GO.win.scrollTop(); // window scroll top

                    APP.GO.lazyLoadThumb();

                },

                // On item click.
                onItemClick: function () {

                    APP.GO.item.on('click', function (e) {

                        e.preventDefault();

                        // variables
                        var $self = $(this),
                            dataIndex = $self.attr('data-index');

                        if ($self.hasClass('x-is-active')) {

                            // close xpanda
                            APP.GO.closeXpanda();

                        } else {

                            // toggle classes
                            APP.GO.item.removeClass('x-is-active').addClass('x-is-not-active');
                            $self.addClass('x-is-active').removeClass('x-is-not-active');

                            // open xpanda
                            APP.GO.openXpanda($self, dataIndex);

                        }

                    });

                },

                // Open xpanda.
                openXpanda: function (target, index) {

                    var desktopScrollTo = defaults.placeholder.desktopScrollTo,
                        mobileScrollTo = defaults.placeholder.mobileScrollTo;

                    // check if mobile device
                    var isMobileDevice,
                        uaString = APP.GO.userAgent.toLowerCase();
                    if (uaString.match(/ipad|iphone|android|iemobile/g)) {
                        isMobileDevice = true;
                    } else {
                        isMobileDevice = false;
                    }

                    // clear placeholder
                    $("." + defaults.placeholder.class).html('').removeClass('x-is-expanded').removeAttr('style');

                    // calculate closest multiple of column count
                    var ceiling = Math.ceil(index / APP.GO.updatedColumnCount) * APP.GO.updatedColumnCount;

                    // what to clone
                    var iContent = target.find(defaults.element.toClone).html();

                    // clone details and add active placeholder styles
                    $("." + defaults.placeholder.class + "[data-until='" + ceiling + "']").html(iContent).addClass('x-is-expanded');
                    APP.GO.styleActivePlaceholder();
                    APP.GO.lazyLoadAsset(index);
                    APP.GO.createButtons(index, ceiling);

                    // scroll to placeholder
                    if (isMobileDevice === true && mobileScrollTo === true) {
                        $('html, body').animate({ scrollTop: target.position().top }, 0, 'linear');
                    } else if (isMobileDevice === false && desktopScrollTo === true) {
                        $('html, body').animate({ scrollTop: target.position().top }, 20, 'linear');
                    }

                    APP.GO.onArrowClick(index);
                    APP.GO.onCloseClick();

                },

                // Close xpanda.
                closeXpanda: function () {

                    // get open x-item
                    var toClose = $('.x-is-active');

                    // clear placeholder
                    $("." + defaults.placeholder.class).html('').removeClass('x-is-expanded').removeAttr('style');

                    // remove classes
                    APP.GO.item.removeClass('x-is-not-active');
                    toClose.removeClass('x-is-active');

                    APP.GO.lazyLoadThumb();

                },

                // Lazy load asset.
                lazyLoadAsset: function (index) {

                    if (APP.GO.container.hasClass('x-lazyload-asset')) {

                        // load placeholder image
                        var clonePlaceHolder = $('.x-is-expanded'),
                            placeholderImageWrap = clonePlaceHolder.find('.x-asset'),
                            lowResImage = placeholderImageWrap.find('img'),
                            highResImage = lowResImage.attr('data-src');

                        if (lowResImage.attr('data-src') && lowResImage.attr('data-src') !== null) {

                            // apply new source of high quality version image
                            lowResImage.attr('src', highResImage);

                            // check if image is done loading
                            lowResImage.one('load', function () {

                                var activeImageWrap = $(defaults.element.selector + '[data-index="' + index + '"]').find('.x-asset');

                                if (!activeImageWrap.hasClass('x-was-loaded')) {
                                    placeholderImageWrap.addClass('x-is-loaded');
                                }

                                // replace source of image
                                var activeImage = activeImageWrap.find('img'),
                                    newActiveString = activeImage.attr('data-src');

                                activeImage.attr('src', newActiveString);
                                activeImageWrap.addClass('x-was-loaded');

                            }).each(function () {
                                if (this.complete) { $(this).trigger('load'); }
                            });

                        }

                    }

                },

                // Lazy load thumbnail.
                lazyLoadThumb: function () {

                    if (APP.GO.container.hasClass('x-lazyload-thumbnail')) {

                        // get placeholder and define window bottom
                        var placeholder = $('.' + defaults.placeholder.class),
                            winBottom = APP.GO.winTop + APP.GO.winHeight;

                        // for each placeholder
                        placeholder.each(function () {

                            // variables
                            var $self = $(this),
                                placeholderTop = $self.offset().top,
                                placeholderBottom = placeholderTop - 100;

                            if (placeholderBottom <= winBottom && placeholderTop >= APP.GO.winTop) {

                                // get thumbnails with data-group equal to placeholder's data-until
                                var dataUntil = $self.attr('data-until'),
                                    inViewThumb = $(defaults.element.selector + '[data-group="' + dataUntil + '"]');

                                // replace source of thumbnail image
                                inViewThumb.each(function () {
                                    var img = $(this).find('> a > img');

                                    // check if image source is done updating
                                    var checkSource = setInterval(function () {
                                        var originalSource = img.attr('src'),
                                            newSource = img.attr('data-src');

                                        if (originalSource === newSource) {
                                            // add class to not show preloader
                                            inViewThumb.addClass('x-is-loaded');
                                            // clear interval
                                            clearInterval(checkSource);
                                        } else {
                                            // replace image src
                                            img.attr('src', newSource);
                                        }
                                    }, 100); // check every 100ms
                                });

                            }

                        });

                    }

                },

                // Create placeholder buttons.
                createButtons: function (index, until) {

                    if (defaults.placeholder.showControls === true) {

                        // variables
                        var leftButton = $('<div class="x-arrow x-prev"></div>'),
                            rightButton = $('<div class="x-arrow x-next"></div>'),
                            closeButton = $('<div class="x-close"></div>');

                        // create buttons
                        $("." + defaults.placeholder.class + "[data-until='" + until + "']").append(leftButton).append(rightButton).append(closeButton);

                        // add disabled classes if first or last child
                        if (index === '1') {
                            $(".x-arrow.x-prev").addClass('x-arrow-disabled');
                        } else if (index === APP.GO.itemLength.toString()) {
                            $(".x-arrow.x-next").addClass('x-arrow-disabled');
                        }

                    }

                },

                // Style active placeholder.
                styleActivePlaceholder: function () {

                    $('.x-is-expanded').css({
                        'margin-top': defaults.placeholder.marginTop,
                        'margin-bottom': 0,
                        'margin-right': defaults.placeholder.marginSides,
                        'margin-left': defaults.placeholder.marginSides,
                        'width': '100%'
                    }).css('width', '-=' + (defaults.placeholder.marginSides * 2));

                },

                // On arrows click.
                onArrowClick: function (index) {

                    $('.x-arrow').on('click', function () {

                        // variables
                        var $self = $(this),
                            destination;

                        // determine which way to navigate
                        if ($self.hasClass('x-prev')) {
                            destination = parseInt(index, 10) - 1;
                        } else {
                            destination = parseInt(index, 10) + 1;
                        }

                        // navigate
                        $(defaults.element.selector + '[data-index="' + destination + '"]').trigger('click', [true]);

                    });

                },

                // On close click.
                onCloseClick: function () {

                    // on close button click
                    $('.x-close').on('click', function () {
                        // close xpanda
                        APP.GO.closeXpanda();
                    });

                },

                // On keyboard click.
                onKeyboardClick: function () {

                    // on keyboard keydown
                    APP.GO.doc.on('keydown', function (e) {

                        // variables
                        var currentActive = $('.x-is-active'),
                            index = currentActive.attr('data-index'),
                            columnCount = APP.GO.updatedColumnCount,
                            destination;

                        if (currentActive.length > 0) {

                            // escape
                            if (e.which === 27) {
                                // close xpanda
                                APP.GO.closeXpanda();
                            }
                            // left
                            if (e.which === 37) {
                                destination = parseInt(index, 10) - 1;
                                $(defaults.element.selector + '[data-index="' + destination + '"]').trigger('click', [true]);
                            }
                            // up
                            if (e.which === 38) {
                                destination = parseInt(index, 10) - columnCount;
                                $(defaults.element.selector + '[data-index="' + destination + '"]').trigger('click', [true]);
                            }
                            // right
                            if (e.which === 39) {
                                destination = parseInt(index, 10) + 1;
                                $(defaults.element.selector + '[data-index="' + destination + '"]').trigger('click', [true]);
                            }
                            // down
                            if (e.which === 40) {
                                destination = parseInt(index, 10) + columnCount;
                                $(defaults.element.selector + '[data-index="' + destination + '"]').trigger('click', [true]);
                            }

                        }

                    });

                }

            };

            // Trigger on document ready.
            APP.GO.globalVar();

            // Trigger only when everything else finished loading.
            APP.GO.win.on('load', function () {
                APP.GO.initialLoad();
                APP.GO.getColumns();
                APP.GO.createPlaceholders();
                APP.GO.styleItems();
                APP.GO.lazyLoadThumb();
                APP.GO.createSpacer();
                APP.GO.addSpacerContent();
            });

            // Trigger on window resize.
            APP.GO.win.on('resize', function () {
                APP.GO.throttleResizeFunc();
            });

            // Trigger on device orientation change.
            APP.GO.win.on('orientationchange', function () {
                // reload page with cache
                window.location.reload(false);
            });

            // Trigger on window scroll.
            APP.GO.win.on('scroll', function () {
                APP.GO.throttleScrollFunc();
            });

            // Trigger when clicked.
            APP.GO.onItemClick();
            APP.GO.onKeyboardClick();

        });

    };

}(jQuery));
